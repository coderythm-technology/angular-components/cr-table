# CrTable

This library was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.2.14.

## Code scaffolding

Run `ng generate component component-name --project cr-table` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module --project cr-table`.
> Note: Don't forget to add `--project cr-table` or else it will be added to the default project in your `angular.json` file. 

## Build

Run `ng build cr-table` to build the project. The build artifacts will be stored in the `dist/` directory.

## Publishing

After building your library with `ng build cr-table`, go to the dist folder `cd dist/cr-table` and run `npm publish`.

## Running unit tests

Run `ng test cr-table` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
