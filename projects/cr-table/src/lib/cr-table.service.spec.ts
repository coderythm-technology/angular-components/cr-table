import { TestBed } from '@angular/core/testing';

import { CrTableService } from './cr-table.service';

describe('CrTableService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CrTableService = TestBed.get(CrTableService);
    expect(service).toBeTruthy();
  });
});
