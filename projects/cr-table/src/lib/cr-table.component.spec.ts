import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CrTableComponent } from './cr-table.component';

describe('CrTableComponent', () => {
  let component: CrTableComponent;
  let fixture: ComponentFixture<CrTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
