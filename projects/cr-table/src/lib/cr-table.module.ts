import { NgModule, ModuleWithProviders } from '@angular/core';
import { CrTableComponent } from './cr-table.component';
import { CrTableService } from './cr-table.service';
// import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// import { RouterModule } from '@angular/router';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatDialog } from '@angular/material/dialog';
import { CrDialogModule } from '@coderythm/cr-dialog';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { CrPaginationModule } from '@coderythm/cr-pagination';
import { CrToasterModule } from '@coderythm/cr-toaster';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [CrTableComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatIconModule,
    MatButtonModule,
    MatTooltipModule,
    MatProgressSpinnerModule,
    CrDialogModule,
    CrPaginationModule,
    CrToasterModule
  ],
  providers: [CrTableService],
  exports: [CrTableComponent]
})
export class CrTableModule {
  static forRoot(config: any): ModuleWithProviders {
    return {
      ngModule: CrTableModule,
      providers: [
        {
          provide: 'config',
          useValue: config
        }
      ]
    };
  }

  static forChild(config: any): ModuleWithProviders {
    return {
      ngModule: CrTableModule,
      providers: [
        {
          provide: 'config',
          useValue: config
        }
      ]
    };
  }
}
