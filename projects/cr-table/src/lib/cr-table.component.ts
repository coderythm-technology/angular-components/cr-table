import {
  Component, OnInit, Input, EventEmitter, Output, OnChanges, AfterViewChecked, ChangeDetectorRef
} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as _ from 'lodash';
import { FormControl } from '@angular/forms';
import { CrTableService } from './cr-table.service';
import { Subject } from 'rxjs';
import { CrToasterService } from '@coderythm/cr-toaster';
import { CrDialogService } from '@coderythm/cr-dialog';

@Component({
  selector: 'cr-table',
  templateUrl: './cr-table.component.html',
  styleUrls: ['./cr-table.component.scss']
})
export class CrTableComponent implements OnInit, OnChanges, AfterViewChecked {
  @Input() type: string;
  @Input() columns;
  @Input() entity;
  @Input() params;
  @Input() appConfig;
  @Input() data: string;
  @Input() hideView = false;
  @Input() hideEdit = false;
  @Input() hideDelete = false;
  @Input() toggleStatus = false;
  @Input() triggerChange: Subject<boolean>;
  @Output() editTrigger: EventEmitter<any> = new EventEmitter();
  @Output() viewTrigger: EventEmitter<any> = new EventEmitter();
  public url;
  myControl = new FormControl();
  displayedColumns: any;
  dataValues = [];
  totalCount = 0;
  defaultTruncate = 50;
  public isLoading = true;

  constructor(
    public http: HttpClient,
    private cdRef: ChangeDetectorRef,
    private toaster: CrToasterService,
    private crTableService: CrTableService,
    private dialog: CrDialogService
  ) {
    this.url = this.crTableService.serverUrl;
  }

  ngOnInit() {
    this.getList();
    if (this.triggerChange) {
      this.triggerChange.subscribe(v => {
        this.getList();
      });
    }
  }

  ngAfterViewChecked() {
    if (this.dataValues) { // check if it change, tell CD update view
      this.cdRef.detectChanges();
    }
  }

  ngOnChanges(event) {
    if (!event.columns.firstChange) {
      this.getList();
    }
  }

  sortingDataAccessor(item, property) {
    if (property.includes('.')) {
      return property.split('.')
        .reduce((object, key) => object[key], item);
    }
    return item[property];
  }

  getList() {
    this.isLoading = true;
    this.dataValues = [];
    this.params.sortOrder = 'descending';
    const call = this.http.get<any>(this.url + '/' + this.entity, { params: this.params })
      .subscribe(
        (res) => {
          this.isLoading = false;
          this.totalCount = res.totalCount;
          this.dataValues = res.body;
          this.displayedColumns = this.columns.map(column => column.name);
        },
        (error: any) => {
          this.isLoading = false;
          console.warn('error', error);
        }
      );
  }

  getRecord(id) {
    this.viewTrigger.emit(id);
  }

  editRecord(id) {
    this.editTrigger.emit(id);
  }

  deleteRecord(id) {
    this.isLoading = true;
    const instance = this.dialog.confirm('Are you sure you want to Delete!', {
      data: {
        buttonYesColor: 'warn',
        buttonNoColor: 'accent'
      }
    });
    instance
      .afterClosed()
      .subscribe(result => {
        if (result) {
          // this.dialog.alert('Ok Deleted');
          this.http
            .delete<any>(this.url + '/' + this.entity + '/' + id)
            .subscribe(
              () => {
                this.toaster.success(`${this.titleCase(this.entity)} Successfully Deleted`);
                this.getList();
              },
              () => {
                this.isLoading = false;
                this.toaster.warning(`Oops!, ${this.titleCase(this.entity)} could not be deleted`);
              }
            );
        } else {
          this.isLoading = false;
        }
      });
  }

  changePage(event) {
    this.params.page = event.currentPage;
    this.params.perPage = event.perPage;
    this.getList();
  }

  titleCase(str) {
    return str.toLowerCase().split(' ').map((word) => {
      return word.replace(word[0], word[0].toUpperCase());
    }).join(' ');
  }

  toggleStatusTrigger(id) {
    this.http.put<any>(this.url + '/' + this.entity + '/status/' + id, {}).
      subscribe(
        () => {
          this.toaster.success(`${this.titleCase(this.entity)} Status Successfully Updated`);
          this.getList();
        },
        () => {
          this.toaster.warning(`Oops!, ${this.titleCase(this.entity)} could not be update status`);
        }
      );
  }

  getFiltered(value, column) {
    if (column && column.lov) {
      column.lov.forEach(lov => {
        if (lov[column.lovId || '_id'] === value) {
          value = lov[column.lovLabel || 'label'];
        }
      });
    }
    return value;
  }
}
