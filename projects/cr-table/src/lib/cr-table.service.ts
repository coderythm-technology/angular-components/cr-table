import { Injectable, Inject } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CrTableService {

  serverUrl = '';

  constructor(@Inject('config') private config: any) {
    if (config.url) {
      this.serverUrl = config.url;
    }
  }
}
