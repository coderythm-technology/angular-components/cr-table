import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpHeaders, HttpRequest, HttpHandler, HttpErrorResponse } from '@angular/common/http';
// import { Router } from '@angular/router';
import { tap } from 'rxjs/operators';
// import { appConfig } from '../../config';

@Injectable({
  providedIn: 'root'
})
export class InterceptorService implements HttpInterceptor {

  public authToken: any;
  public httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json/multipart/form-data',
      Authorization: this.authToken
    })
  };

  constructor() { }

  intercept(req: HttpRequest<any>, next: HttpHandler) {
    // Get the auth token from the service.
    this.authToken = this.getAuthorizationToken();
    // Clone the request and replace the original headers with
    // cloned headers, updated with the authorization.
    // if (this.authToken) {
    const authReq = req.clone({
      headers: req.headers.set('Authorization', this.authToken)
    });

    return next.handle(authReq).pipe(
      tap(
        (event) => { },
        (error: any) => {
          if (error instanceof HttpErrorResponse) {
            if (error.status === 401) {
              this.clearToken();
            } else if (error.error.code === 11000) {
              // this.toaster.error('Duplicate Already Exists', 'Error');
            }
          }
        }
      )
    );
    // }
    // send cloned request with header to the next handler.
  }

  getAuthorizationToken() {
    this.authToken = localStorage.getItem('TASLEEM_DEV' + '_id_token');
    return this.authToken ? 'Bearer ' + this.authToken : '';
  }
  clearToken() {
    localStorage.removeItem('TASLEEM_DEV' + '_id_token');
    // this.router.navigate(['/']);
  }
}
