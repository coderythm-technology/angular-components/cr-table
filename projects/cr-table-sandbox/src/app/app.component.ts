import { Component } from '@angular/core';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'cr-table-sandbox';

  params = {
    page: 0,
    perPage: 10,
    range: {
      startDate: '',
      endDate: ''
    },
    search: '',
    sort: 'createdAt ',
    sortOrder: 'descending',
    tags: []
  };
  appConfig = {
    appName: 'Tasleem',
    appKey: 'TASLEEM_DEV',
    version: '0.0.1',
    accessPath: 'access',
    isRegisterAllowed: false,
    isForgotPasswordAllowed: true,
    appHomePath: '/app/dashboard',
    perPageOptions: [10, 50, 100, 250]
  };
  pages = [];

  vehicleTypes = [
    {
        _id: 'bike',
        label: 'Bike'
    },
    {
        _id: 'van',
        label: 'Refrigerated Van'
    },
    {
        _id: 'car',
        label: 'Car'
    }
];

  // columns = [
  //   { name: 'name', label: 'Name', type: 'i18n', singleLine: true },
  //   { name: 'description', label: 'Description', type: 'i18n', htmlContent: true, truncateTo: 100, prefix: '+971 ', postfix: '\'s' },
  //   // { name: 'description', label: 'Description', type: 'i18n', htmlContent: true },
  //   { name: 'status', label: 'Status' },
  //   { name: 'action', label: 'Action', type: 'action' },
  // ];

  columns = [
    { name: 'firstName', label: 'First Name' },
    { name: 'lastName', label: 'Last Name' },
    { name: 'email', label: 'Email' },
    { name: 'number', label: 'Number' },
    { name: 'isWorking', label: 'Is Working', booleanIcon: true },
    // { name: 'onBoardStatus', label: 'On Board Status' },
    { name: 'deliveryTypes', label: 'Delivery Type', lov: this.vehicleTypes },
    { name: 'createdAt', label: 'Date', header: 'metadata', date: true, dateFormat: 'short' },
    { name: 'isActive', label: 'isActive', header: 'metadata', booleanIcon: true },
    { name: 'action', label: 'Action', type: 'action' },
  ];

  triggerTableValue: Subject<boolean> = new Subject();

  editClicked() {
    console.log('edit clicked');
  }

  viewClicked() {
    console.log('view clicked');
  }

  triggerTable() {
    this.triggerTableValue.next(true);
  }
}
